#include <string>
#include <vector>
using namespace std;

/*
O(N^3) time limited exceeded
optimized to O(N^2logN)
*/

int substringDiff(int k, string s1, string s2)
{
	int N1 = s1.length(), N2 = s2.length();
	vector<vector<int>> dp(N1 + 1, vector<int>(N2 + 1));
	for (int i = 0; i < N1; ++i)
	{
		for (int j = 0; j < N2; ++j)
		{
			if (s1[i] != s2[j])
				dp[i + 1][j + 1] = dp[i][j] + 1;
			else
				dp[i + 1][j + 1] = dp[i][j];
		}
	}

	int res = 0;
	for (int i = 0; i < N1; ++i)
	{
		for (int j = 0; j < N2; ++j)
		{
			int lo = 0, hi = min(i, j) + 1;
			while (lo < hi)
			{
				int mi = (hi - lo) / 2 + lo;
				if (dp[i + 1][j + 1] - dp[i - mi][j - mi] <= k)
					lo = mi + 1;
				else
					hi = mi;
			}
			res = max(res, lo);
		}
	}
	return res;
}