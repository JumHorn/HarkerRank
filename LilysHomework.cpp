#include <algorithm>
#include <unordered_map>
#include <vector>
using namespace std;

int numberofSwap(vector<int> arr, vector<int> &target)
{
	int N = arr.size(), res = 0;
	unordered_map<int, int> m; //{val,index}
	for (int i = 0; i < N; ++i)
		m[arr[i]] = i;
	for (int i = 0; i < N; ++i)
	{
		if (arr[i] != target[i])
		{
			int index = m[target[i]];
			m[arr[i]] = index;
			swap(arr[index], arr[i]);
			++res;
		}
	}
	return res;
}

int lilysHomework(vector<int> arr)
{
	vector<int> dup = arr;
	sort(dup.begin(), dup.end());
	int res = numberofSwap(arr, dup);
	reverse(dup.begin(), dup.end());
	res = min(res, numberofSwap(arr, dup));
	return res;
}