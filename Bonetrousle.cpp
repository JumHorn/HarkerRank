#include <numeric>
#include <vector>
using namespace std;

/*
避免long long乘法越界
被卡精度，卡到不想写代码了

乘法卡精度，改成用加法加溢出判断了
*/

vector<long> bonetrousle(long long n, long long k, long long b)
{
	// long long lo=(1+b)*b/2,hi=(k-b+1+k)*b/2;
	// if(n<lo||n>hi)
	if (n < (1 + b) * b / 2 || n * 1.0 / b > (k - b + 1 + k) / 2.0)
		return {-1};
	long long sum = 0, i = 0;
	for (; i < b && sum < n; ++i)
		sum += k - i;
	if (i == b && sum < n)
		return {-1};

	long long lo = 1, hi = k + 1; //this time they refer to index not sum
	while (lo < hi)
	{
		long long mi = (hi - lo) / 2 + lo;
		// if((mi+mi+b-1)*b/2<n)
		sum = 0;
		for (i = 0; i < b && sum <= n; ++i)
			sum += mi + i;

		if (sum <= n)
			lo = mi + 1;
		else
			hi = mi;
	}
	--lo;
	vector<long> res;
	for (int i = 0; i < b; ++i)
	{
		res.push_back(lo + i);
		n -= lo + i;
	}
	long long offset = b - n;
	for (int i = offset; i < b; ++i)
		++res[i];
	return res;
}