#include <string>
#include <vector>
using namespace std;

// const value
static const int MOD = 1e9 + 7;
static const int SHIFTL = 314159;

int xorAndSum(string a, string b)
{
	int alen = a.length(), blen = b.length();
	int N = max(alen, blen + SHIFTL);

	//add prefix zero
	a = string(max(N - alen, 0), '0') + a;
	b = string(max(N - blen, 0), '0') + b;

	//precalculate pow of 2 MOD
	vector<long long> pow2(N + 1);
	pow2[0] = 1;
	for (int i = 0; i < N; ++i)
		pow2[i + 1] = (pow2[i] << 1) % MOD;

	//prefix sum 1
	vector<int> prefix(N + 1);
	for (int i = 0; i < N; ++i)
		prefix[i + 1] = prefix[i] + b[i] - '0';

	long long res = 0;
	for (int i = 0; i < N; ++i)
	{
		int n = min(N - i, SHIFTL + 1);
		int ones = prefix[i + n] - prefix[i];
		if (a[i] == '0')
			res = (res + (ones * pow2[N - i - 1]) % MOD) % MOD;
		else
			res = (res + (SHIFTL + 1 - ones) * pow2[N - i - 1] % MOD) % MOD;
	}
	return res;
}