#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <vector>
using namespace std;

int main()
{
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
	const std::size_t SIZE = (1u << 31), BITSIZE = SIZE / 4 / 8;
	static unsigned int bit[BITSIZE];
	memset((int *)bit, 0, sizeof(bit));
	int N, S, P, Q;
	cin >> N >> S >> P >> Q;
	long long a = S;
	bit[a / 8] ^= 1 << (a & 7);
	int res = 1;
	for (int i = 1; i < N; ++i)
	{
		a = ((a * P + Q) & (SIZE - 1));
		if ((bit[a / 8] & (1 << (a & 7))) == 0)
		{
			++res;
			bit[a / 8] ^= 1 << (a & 7);
		}
	}
	cout << res << endl;
	return 0;
}
