#include <queue>
#include <string>
#include <vector>
using namespace std;

vector<string> split(const string &s, const string &sep = " ")
{
	int N = s.length();
	bool delimter[128] = {0};
	for (auto c : sep)
		delimter[c] = true;
	vector<string> res;
	for (int i = 0, j = 0; i <= N; ++i)
	{
		if (i == N || delimter[s[i]])
		{
			res.push_back(s.substr(j, i - j));
			j = i + 1;
		}
	}
	return res;
}

/*
 * Complete the 'shop' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER n
 *  2. INTEGER k
 *  3. STRING_ARRAY centers
 *  4. 2D_INTEGER_ARRAY roads
 */

void bfs(vector<vector<pair<int, int>>> &graph, vector<int> &fishes, vector<vector<int>> &dp)
{
	queue<pair<int, int>> q; //{node,fish type mask}
	q.push({1, fishes[1]});
	while (!q.empty())
	{
		auto front = q.front();
		int node = front.first, fish = front.second;
		q.pop();
		for (auto &g : graph[node])
		{
			int to = g.first, w = g.second;
			int f = fish | fishes[to];
			if (dp[to][f] > dp[node][fish] + w)
			{
				dp[to][f] = dp[node][fish] + w;
				q.push({to, f});
			}
		}
	}
}

int shop(int n, int k, vector<string> centers, vector<vector<int>> roads)
{
	//{dest,time}
	vector<vector<pair<int, int>>> graph(n + 1);
	for (auto &r : roads)
	{
		graph[r[0]].push_back({r[1], r[2]});
		graph[r[1]].push_back({r[0], r[2]});
	}
	//minimum time to get to center m with bitmask type fishes
	vector<vector<int>> dp(n + 1, vector<int>(1 << k, INT_MAX));
	vector<int> fishes = {0}; //type of fishes each center sell, 0 reversed
	for (auto &str : centers)
	{
		auto v = split(str);
		int mask = 0;
		for (int i = 1; i < v.size(); ++i)
		{
			int type = stoi(v[i]);
			mask |= (1 << (type - 1));
		}
		fishes.push_back(mask);
	}
	dp[1][fishes[1]] = 0;
	bfs(graph, fishes, dp);

	int res = INT_MAX, mask = (1 << k) - 1;
	for (int i = 0; i <= mask; ++i)
	{
		for (int j = i; j <= mask; ++j)
		{
			if ((i | j) == mask)
				res = min(res, max(dp[n][i], dp[n][j]));
		}
	}
	return res;
}