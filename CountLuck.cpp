#include <string>
#include <vector>
using namespace std;

pair<int, int> findStart(vector<string> &matrix)
{
	int M = matrix.size(), N = matrix[0].size();
	for (int i = 0; i < M; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			if (matrix[i][j] == 'M')
				return {i, j};
		}
	}
	return {-1, -1};
}

bool backTracking(vector<string> &matrix, int r, int c)
{
	int M = matrix.size(), N = matrix[0].size();
	//board dfs direction
	int path[5] = {-1, 0, 1, 0, -1};
	for (int i = 0; i < 4; ++i)
	{
		int x = r + path[i], y = c + path[i + 1];
		if (x >= 0 && x < M && y >= 0 && y < N)
		{
			if (matrix[x][y] == '*')
				return true;
			if (matrix[x][y] == '.')
			{
				matrix[x][y] = '#';
				if (backTracking(matrix, x, y))
					return true;
				matrix[x][y] = '.';
			}
		}
	}
	return false;
}

int checkPath(vector<string> &matrix, int r, int c)
{
	int M = matrix.size(), N = matrix[0].size(), res = 0;
	//board dfs direction
	int path[5] = {-1, 0, 1, 0, -1};
	int count = 0;
	for (int i = 0; i < 4; ++i)
	{
		int x = r + path[i], y = c + path[i + 1];
		if (x >= 0 && x < M && y >= 0 && y < N && matrix[x][y] == '.')
			++count;
	}
	if (count > 0)
		++res;
	matrix[r][c] = '%'; //already visited
	for (int i = 0; i < 4; ++i)
	{
		int x = r + path[i], y = c + path[i + 1];
		if (x >= 0 && x < M && y >= 0 && y < N && matrix[x][y] == '#')
		{
			res += checkPath(matrix, x, y);
			break;
		}
	}
	return res;
}

string countLuck(vector<string> matrix, int k)
{
	auto pos = findStart(matrix);
	backTracking(matrix, pos.first, pos.second);

	if (k == checkPath(matrix, pos.first, pos.second))
		return "Impressed";
	return "Oops!";
}