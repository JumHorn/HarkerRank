#include <algorithm>
#include <vector>
using namespace std;

/*
最终结果一定是每次给n-1个人增加1需要的次数，无需考虑2，5的增加方法

为了问题描述简单
首先将该问题等价代换为，每次给n-1个人增加1，变为
每次给1个人减少1，知道最终结果一样

那么每次减少1，相当于所有人减少到最小值，那么操作步骤为
operations=sum-min*N

问题1
为什么不减少多一点，使得步骤更少
如果减少总次数n

当n>5时，只要考虑n=n%5的情况
当n=1,2时，只要1次，已经最优
当n=3,4时，不用5，只要2次即可，用5完成，最少也要两次，所以减少到最小值是最优的方案


test case 所有人都减少到-1比减少到0次数少
0 4 4 4 4  0+2+2+2+2
		   1+1+1+1+1

0 3 3 3 3 所有人都减少到-2
0 2 2 2 2 所有人都减少到-3
*/

int equal(vector<int> arr)
{
	int minval = *min_element(arr.begin(), arr.end());
	int res = INT_MAX;
	for (int offset = 0; offset <= 3; ++offset)
	{
		int operations = 0;
		for (auto n : arr)
		{
			int d = n - minval + offset;
			operations += d / 5;
			d %= 5;
			if (d == 0)
				continue;
			operations += (d <= 2 ? 1 : 2);
		}
		res = min(res, operations);
	}
	return res;
}