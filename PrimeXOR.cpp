#include <vector>
using namespace std;

/*
无论是什么级别的程序猿对于计算顺序卡bug是常见的
这个bug也是卡了我3个多小时，又一次要崩溃
这两者交换一下顺序，结果完全不一样，在数据不可完全调试的情况下
非常难以解决该问题
dp[i][j] = ((count[i] + 2) / 2 * dp[i - 1][j] % MOD + sum) % MOD;
dp[i][j] = (dp[i - 1][j] * (count[i] + 2) / 2 % MOD + sum) % MOD;
*/

// const value
static const int MOD = 1e9 + 7;

// generate prime in [2,n]
vector<int> generatePrime(int n)
{
	assert(n > 1);
	vector<bool> primer(n + 1);
	for (int i = 2; i <= n; ++i)
	{
		if (primer[i])
			continue;
		for (int j = i + i; j <= n; j += i)
			primer[j] = true;
	}
	vector<int> res;
	for (int i = 2; i <= n; ++i)
	{
		if (!primer[i])
			res.push_back(i);
	}
	return res;
}

int primeXor(vector<int> a)
{
	// 3500<=a[i]<=4500
	const int BASE = 3500, N = 4500 - BASE + 1, M = (1 << 13); //max(all xor set)<8192
	static const vector<int> primer = generatePrime(M);		   //init only once

	static long dp[N][M], count[N];
	memset(dp, 0, sizeof(dp));
	memset(count, 0, sizeof(count));
	for (auto n : a)
		++count[n - BASE];
	dp[0][0] = (count[0] + 2) / 2;
	dp[0][BASE] = (count[0] + 1) / 2;
	for (int i = 1; i < N; ++i)
	{
		for (int j = 0; j < M; ++j)
		{
			long sum = (count[i] + 1) / 2 * dp[i - 1][j ^ (i + BASE)] % MOD;
			dp[i][j] = ((count[i] + 2) / 2 * dp[i - 1][j] % MOD + sum) % MOD;
		}
	}

	long res = 0;
	for (auto n : primer)
		res = (res + dp[N - 1][n]) % MOD;
	return res;
}