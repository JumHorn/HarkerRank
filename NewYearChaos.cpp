#include <iostream>
#include <vector>
using namespace std;

//Fenwick tree(BIT)
class Fenwick
{
public:
	Fenwick(int size) : tree(size + 1) {}

	int sum(int index) const
	{
		int res = 0;
		for (++index; index > 0; index -= index & -index)
			res += tree[index];
		return res;
	}

	void update(int index, int delta)
	{
		int N = tree.size();
		for (++index; index < N; index += index & -index)
			tree[index] += delta;
	}

private:
	vector<int> tree;
};
/********end of Fenwick tree(BIT)********/

void minimumBribes(vector<int> q)
{
	int N = q.size(), res = 0;
	// optimized with BIT
	// for (int i = 0; i < N - 1; ++i)
	// {
	// 	int count = 0;
	// 	for (int j = i + 1; j < N; ++j)
	// 	{
	// 		if (q[i] > q[j])
	// 			++count;
	// 	}
	// 	if (count > 2)
	// 	{
	// 		cout << "Too chaotic" << endl;
	// 		return;
	// 	}
	// 	res += count;
	// }
	// cout << res << endl;

	Fenwick bit(N + 1);
	for (auto n : q)
		bit.update(n, 1);
	for (int i = 0; i < N; ++i)
	{
		bit.update(q[i], -1);
		int count = bit.sum(q[i]);
		if (count > 2)
		{
			cout << "Too chaotic" << endl;
			return;
		}
		res += count;
	}
	cout << res << endl;
}