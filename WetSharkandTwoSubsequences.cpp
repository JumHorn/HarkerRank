#include <vector>
using namespace std;

/*
数组A,B必须要求长度相同，动态规划一开始没有考虑到长度这个因素，所以要把长度加上
dp[sum][count]表示由count个元素组成的sum有多少种

人话是: dp[3][2]表示由2个元素组成和为3的组合有多少种
*/

// const value
static const int MOD = 1e9 + 7;

vector<vector<long>> countSubequenceSum(vector<int> &arr, int sum)
{
	int N = arr.size();
	vector<vector<long>> dp(sum + 1, vector<long>(N + 1));
	dp[0][0] = 1;
	for (int i = 0; i < N; ++i)
	{
		for (int j = sum; j >= arr[i]; --j)
		{
			for (int k = 1; k <= N; ++k)
				dp[j][k] = (dp[j][k] + dp[j - arr[i]][k - 1]) % MOD;
		}
	}
	return dp;
}

int twoSubsequences(vector<int> x, int r, int s)
{
	if (r == 0 || r < s || (r + s) % 2 == 1 || (r - s) % 2 == 1)
		return 0;
	long long sum1 = (r + s) / 2, sum2 = (r - s) / 2, res = 0, N = x.size();
	auto dp1 = countSubequenceSum(x, sum1);
	auto dp2 = countSubequenceSum(x, sum2);
	for (int i = 0; i <= N; ++i)
		res = (res + dp1[sum1][i] * dp2[sum2][i] % MOD) % MOD;
	return res;
}