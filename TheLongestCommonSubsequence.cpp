#include <algorithm>
#include <vector>
using namespace std;

vector<int> longestCommonSubsequence(vector<int> a, vector<int> b)
{
	int alen = a.size(), blen = b.size();
	vector<vector<int>> dp(alen + 1, vector<int>(blen + 1));
	for (int i = 0; i < alen; ++i)
	{
		for (int j = 0; j < blen; ++j)
		{
			if (a[i] == b[j])
				dp[i + 1][j + 1] = dp[i][j] + 1;
			else
				dp[i + 1][j + 1] = max(dp[i + 1][j], dp[i][j + 1]);
		}
	}
	vector<int> res;
	for (int i = alen - 1, j = blen - 1; i >= 0 && j >= 0;)
	{
		if (a[i] == b[j])
		{
			res.push_back(a[i]);
			--i;
			--j;
		}
		else
			(dp[i + 1][j] >= dp[i][j + 1]) ? --j : --i;
	}
	reverse(res.begin(), res.end());
	return res;
}