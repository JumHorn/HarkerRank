#include <vector>
using namespace std;

//Turan's theorem(托兰定理)
// n vertices(顶点数) r clique(图中的最大团)
// 返回该图的最大边数
int turan(int n, int r)
{
	return (1 - 1.0 / r) * n * n / 2;
}