#include <vector>
using namespace std;

/*
prefixsum下标和arr下标是最容易出错的地方
*/

int lower(vector<long long> &prefixsum, int left, int right, long long sum)
{
	int lo = left + 1, hi = right + 1; //using prefixsum index
	while (lo < hi)
	{
		int mi = (hi - lo) / 2 + lo;
		if (prefixsum[mi] - prefixsum[left] < sum)
			lo = mi + 1;
		else
			hi = mi;
	}
	return lo - 1; //change to arr index
}

int upper(vector<long long> &prefixsum, int left, int right, long long sum)
{
	int lo = left + 1, hi = right + 1; //using prefixsum index
	while (lo < hi)
	{
		int mi = (hi - lo) / 2 + lo;
		if (prefixsum[mi] - prefixsum[left] <= sum)
			lo = mi + 1;
		else
			hi = mi;
	}
	return lo - 1; //change to arr index
}

//[left,right)
int divide(vector<long long> &prefixsum, int left, int right)
{
	if (right - left <= 1)
		return 0;
	long long sum = prefixsum[right] - prefixsum[left];
	//all [left,right) subarr value are 0
	if (sum == 0)
		return right - left - 1;
	if (sum % 2 == 1)
		return 0;
	sum /= 2;
	int l = 0, r = 0, res = 0;
	auto it1 = lower(prefixsum, left, right, sum);
	if (it1 != right && prefixsum[it1 + 1] - prefixsum[left] == sum)
	{
		res = 1;
		r = divide(prefixsum, it1 + 1, right);
	}
	auto it2 = upper(prefixsum, left, right, sum);
	if (it2 != right && prefixsum[it2] - prefixsum[left] == sum)
	{
		res = 1;
		l = divide(prefixsum, left, it2);
	}
	return res + max(l, r);
}

int arraySplitting(vector<int> arr)
{
	int N = arr.size();
	vector<long long> prefixsum = {0};
	for (int i = 0; i < N; ++i)
		prefixsum.push_back(prefixsum.back() + arr[i]);
	return divide(prefixsum, 0, N);
}