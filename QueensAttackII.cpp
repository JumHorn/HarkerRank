#include <vector>
using namespace std;

int queensAttack(int n, int k, int r_q, int c_q, vector<vector<int>> obstacles)
{
	// (-1,1). (0,1) (1,1)
	// (-1,0). (0,0) (1,0)
	// (-1,-1).(0,-1) (1,-1)
	// index+1 avoid negative index
	vector<vector<int>> dir(3, vector<int>(3));
	dir[0][2] = min(c_q - 1, n - r_q), dir[1][2] = n - r_q, dir[2][2] = min(n - r_q, n - c_q);
	dir[0][1] = c_q - 1, dir[2][1] = n - c_q;
	dir[0][0] = min(c_q, r_q) - 1, dir[1][0] = r_q - 1, dir[2][0] = min(r_q - 1, n - c_q);

	for (auto &v : obstacles)
	{
		int r = v[0], c = v[1];
		int r_len = abs(r - r_q), c_len = abs(c - c_q);
		if (r_len == c_len || r_len == 0 || c_len == 0)
		{
			int len = max(r_len, c_len), x = (c - c_q) / len, y = (r - r_q) / len;
			dir[x + 1][y + 1] = min(dir[x + 1][y + 1], len - 1);
		}
	}

	int res = 0;
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
			res += dir[i][j];
	}
	return res;
}