#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <map>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_set>
#include <vector>
using namespace std;

class HrmlObject
{
public:
	~HrmlObject()
	{
		for (auto &obj : data)
		{
			if (obj.second != nullptr)
				delete obj.second;
		}
	}

	HrmlObject *getObj(const string &tag)
	{
		auto it = data.find(tag);
		if (it != data.end())
			return it->second;
		return nullptr;
	}

	string getAttribute(const string &name)
	{
		auto it = attrib.find(name);
		if (it != attrib.end())
			return it->second;
		return "";
	}

	void addData(string &tag, HrmlObject *obj)
	{
		data[tag] = obj;
	}

	void addAttribute(string &name, string &value)
	{
		attrib[name] = value;
	}

private:
	map<string, HrmlObject *> data; //nested tags
	map<string, string> attrib;		//attribute
};

string valueParse(string &token)
{
	if (token.back() == '>')
		token.pop_back();
	if (token.back() == '"')
		token.pop_back();
	return token.substr(1);
}

vector<string> split(const string &s, const string &sep)
{
	int N = s.length();
	vector<string> res;
	unordered_set<char> delimter(sep.begin(), sep.end());
	for (int i = 0, j = 0; i <= N; ++i)
	{
		if (i == N || delimter.count(s[i]) != 0)
		{
			res.push_back(s.substr(j, i - j));
			j = i + 1;
		}
	}
	return res;
}

void hrmlParse(string hrml, HrmlObject &db)
{
	stringstream ss(hrml);
	stack<HrmlObject *> s;
	s.push(&db);
	string token, name;
	bool is_name = false;
	while (ss >> token)
	{
		if (token[0] == '<') //tag
		{
			if (token[1] != '/') //begin token
			{
				string tag = valueParse(token);
				HrmlObject *obj = new HrmlObject();
				s.top()->addData(tag, obj);
				s.push(obj);
			}
			else //end token
				s.pop();
		}
		else // attribute
		{
			if (token == "=")
				is_name = true;
			else
			{
				if (is_name)
				{
					string value = valueParse(token);
					s.top()->addAttribute(name, value);
					is_name = false;
				}
				else
				{
					int index = 0, size = token.size();
					while (index < size && token[index] != '=')
						++index;
					if (index == size) //name = "value"
						name = token;
					else //name="value"
					{
						name = token.substr(0, index);
						string value = token.substr(index + 1);
						value = valueParse(value);
						s.top()->addAttribute(name, value);
					}
				}
			}
		}
	}
}

int main()
{
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
	int N, Q;
	cin >> N >> Q;
	cin.ignore(); //ignore last \n
	string line;
	string hrml;
	for (int i = 0; i < N; ++i)
	{
		getline(cin, line);
		hrml += line + "\n";
	}

	//parse hrml database
	HrmlObject db;
	hrmlParse(hrml, db);

	string q;
	for (int i = 0; i < Q; ++i)
	{
		cin >> q;
		vector<string> tokens = split(q, ".~");
		int size = tokens.size(), j = 0;
		HrmlObject *obj = &db;
		for (; j < size - 1; ++j)
		{
			obj = obj->getObj(tokens[j]);
			if (obj == nullptr)
				break;
		}
		if (j == size - 1 && obj != nullptr)
		{
			string attrib = obj->getAttribute(tokens[j]);
			if (attrib == "")
				cout << "Not Found!" << endl;
			else
				cout << attrib << endl;
		}
		else
			cout << "Not Found!" << endl;
	}
	return 0;
}
