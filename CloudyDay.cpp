#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

/*
思路：
将每朵云的单独覆盖区间计算出来，就是这朵云不和其他云公共覆盖的区间
然后再计算单独覆盖区间的人口数量

bug处理：
这个问题遇到了bug，testcase有200000的大数据
但是我还是从复杂的数据中找到了代码的bug
使用暴力破解方法，计算testcase数据，确定我代码的哪一个数据计算错了
将局部数据打印出来，再分析代码出错原因

该bug具体是区间分解时，错过了一个特殊情况,下一个区间很小
range问题多bug，光想是不能解决问题的，要画图之后才能理解
|---------|
	|-----------|
	  |--------------------|
	     |----|
*/

//population of town,location of town, location of clouds, range of clouds
long long maximumPeople(vector<long long> p, vector<long long> x, vector<long long> y, vector<long long> r)
{
	vector<pair<long long, long long>> range, town; //{x,p}
	int TownLen = p.size(), CloudLen = y.size();
	for (int i = 0; i < TownLen; ++i)
		town.push_back({x[i], p[i]});
	sort(town.begin(), town.end());

	for (int i = 0; i < CloudLen; ++i)
		range.push_back({y[i] - r[i], y[i] + r[i]});
	sort(range.begin(), range.end(), [&](pair<long long, long long> &lhs, pair<long long, long long> &rhs)
		 {
			 if (lhs.first != rhs.first)
				 return lhs.first < rhs.first;
			 return lhs.second > rhs.second;
		 });

	//get only one cloud range
	vector<vector<pair<long long, long long>>> onlyAffectedRange; //range only covered by one cloud
	long long l = range[0].first;
	for (int i = 0, j = 0; i < CloudLen; i = j)
	{
		bool flag = false;
		for (j = i + 1; j < CloudLen && range[j].second <= range[i].second; ++j)
		{
			if (range[j].first - 1 >= l)
			{
				if (!flag)
				{
					onlyAffectedRange.emplace_back();
					flag = true;
				}
				onlyAffectedRange.back().push_back({l, range[j].first - 1});
			}
			l = max(l, range[j].second + 1);
		}
		long long l1 = range[i].second;
		if (j < CloudLen)
			l1 = min(l1, range[j].first - 1);
		if (l <= l1)
		{
			if (!flag)
			{
				onlyAffectedRange.emplace_back();
				flag = true;
			}
			onlyAffectedRange.back().push_back({l, l1});
		}
		//update left bound
		l = range[i].second + 1;
		if (j < CloudLen)
			l = max(l, range[j].first);
	}

	long long sunny = 0; //already sunny town
	for (int i = 0, j = 0; i < TownLen; ++i)
	{
		while (j < range.size() && town[i].first > range[j].second)
			++j;
		if (j == range.size() || town[i].first < range[j].first)
			sunny += town[i].second;
	}

	//get one cloud covered max population
	long long cloudy = 0, t = 0; //index of town
	for (auto &v : onlyAffectedRange)
	{
		long long onecloud = 0;
		if (t == TownLen)
			break;
		for (auto &r : v)
		{
			for (; t < TownLen && town[t].first <= r.second; ++t)
			{
				if (town[t].first >= r.first)
					onecloud += town[t].second;
			}
		}
		cloudy = max(cloudy, onecloud);
	}
	return sunny + cloudy;
}