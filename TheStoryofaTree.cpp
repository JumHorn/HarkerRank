#include <set>
#include <string>
#include <vector>
using namespace std;

int gcd(int x, int y)
{
	return x == 0 ? y : gcd(y % x, x);
}

void checkGuess(vector<vector<int>> &graph, int at, int from, set<pair<int, int>> &wrong, set<pair<int, int>> &right)
{
	int res = 0;
	for (auto to : graph[at])
	{
		if (to != from)
		{
			auto it = wrong.find({at, to});
			if (it != wrong.end())
			{
				right.insert({at, to});
				wrong.erase(it);
			}
			checkGuess(graph, to, at, wrong, right);
		}
	}
}

int dfs(vector<vector<int>> &graph, int at, int from, set<pair<int, int>> &wrong, set<pair<int, int>> &right, int k)
{
	int res = 0;
	if (right.size() >= k)
		++res;
	for (auto to : graph[at])
	{
		if (to != from)
		{
			//reverse edge at->to
			auto it = right.find({at, to});
			if (it != right.end())
			{
				wrong.insert({at, to});
				right.erase(it);
			}
			it = wrong.find({to, at});
			if (it != wrong.end())
			{
				right.insert({to, at});
				wrong.erase(it);
			}
			res += dfs(graph, to, at, wrong, right, k);
			//reverse back edge to->at
			it = right.find({to, at});
			if (it != right.end())
			{
				wrong.insert({to, at});
				right.erase(it);
			}
			it = wrong.find({at, to});
			if (it != wrong.end())
			{
				right.insert({at, to});
				wrong.erase(it);
			}
		}
	}
	return res;
}

string storyOfATree(int n, vector<vector<int>> edges, int k, vector<vector<int>> guesses)
{
	vector<vector<int>> graph(n + 1);
	for (auto &edge : edges)
	{
		graph[edge[0]].push_back(edge[1]);
		graph[edge[1]].push_back(edge[0]);
	}
	set<pair<int, int>> wrong, right;
	for (auto &g : guesses)
		wrong.insert({g[0], g[1]});
	int res = 0;
	checkGuess(graph, 1, -1, wrong, right);
	res += dfs(graph, 1, -1, wrong, right, k);
	int g = gcd(res, n);
	if (res == 0)
		return string("0/1");
	return to_string(res / g) + "/" + to_string(n / g);
}