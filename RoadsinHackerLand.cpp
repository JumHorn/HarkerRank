#include <string>
#include <vector>
using namespace std;

/*
为什么是最小生成树
每一个节点是2^n
2^(n+1)要大于2^n + 2^(n-1) + ... 2^1
所以最小生成树的路径，就是两个节点间的最短路径
*/

/*
因为一个数据越界，查了有两个小时
long long 转为 int型 carry 发生自动转时出错，已经不是一次了
对于10的5次方相乘int也是越界，这种细节已经卡了我很多次了
*/

//DSU minimum version
class DSU
{
public:
	DSU(int size) : parent(size)
	{
		for (int i = 0; i < size; ++i)
			parent[i] = i;
	}

	int Find(int x)
	{
		if (x != parent[x])
			parent[x] = Find(parent[x]);
		return parent[x];
	}

	bool Union(int x, int y)
	{
		int xp = Find(x), yp = Find(y);
		if (xp == yp)
			return false;
		parent[yp] = xp;
		return true;
	}

private:
	vector<int> parent;
};
/********end of DSU minimum version********/

//return number of node in the subtree
int postorder(vector<vector<pair<int, int>>> &graph, int n, int at, int from, vector<long long> &weight)
{
	int res = 1;
	for (auto p : graph[at])
	{
		int to = p.first, w = p.second;
		if (to == from)
			continue;
		int count = postorder(graph, n, to, at, weight);
		weight[w] = (long long)count * (n - count);
		res += count;
	}
	return res;
}

string valueTransfom(vector<long long> &bin)
{
	string res;
	long long carry = 0, N = bin.size();
	for (int i = 0; i < N || carry != 0; ++i)
	{
		if (i < N)
			carry += bin[i];
		res.push_back('0' + carry % 2);
		carry >>= 1;
	}
	while (!res.empty() && res.back() == '0')
		res.pop_back();
	reverse(res.begin(), res.end());
	return res;
}

string roadsInHackerland(int n, vector<vector<int>> roads)
{
	sort(roads.begin(), roads.end(), [&](vector<int> &lhs, vector<int> &rhs)
		 { return lhs[2] < rhs[2]; });
	vector<vector<pair<int, int>>> graph(n + 1); //{node,weight}
	DSU dsu(n + 1);
	for (auto &r : roads)
	{
		if (dsu.Union(r[0], r[1]))
		{
			graph[r[0]].push_back({r[1], r[2]});
			graph[r[1]].push_back({r[0], r[2]});
		}
	}
	int M = roads.size();
	vector<long long> res(M);
	postorder(graph, n, 1, -1, res);
	return valueTransfom(res);
}