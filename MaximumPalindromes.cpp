#include <string>
#include <unordered_map>
#include <vector>
using namespace std;

// const value
static const int MOD = 1e9 + 7;

//prefixsum of each alpha
vector<vector<int>> prefixsum;

// factorial
vector<long long> fac, ifac;

// pow with mod
long long modPow(long long x, int n)
{
	static const int MOD = 1e9 + 7;
	long long res = 1;
	for (auto i = n; i > 0; i /= 2)
	{
		if (i % 2)
			res = res * x % MOD;
		x = x * x % MOD;
	}
	return res;
}

// factorial stuff
void produceFact(int n)
{
	static const int MOD = 1e9 + 7;
	// Pre-process fac and inverse fac.
	fac.resize(n + 1, 1), ifac.resize(n + 1, 1);
	for (int i = 2; i <= n; ++i)
	{
		fac[i] = fac[i - 1] * i % MOD;
		ifac[i] = modPow(fac[i], MOD - 2);
	}
}

void initialize(string s)
{
	// This function is called once before all queries.
	int N = s.length();
	prefixsum.resize(26);
	produceFact(N);
	for (char c = 'a'; c <= 'z'; ++c)
	{
		int index = c - 'a';
		prefixsum[index].push_back(0);
		for (int i = 0; i < N; ++i)
			prefixsum[index].push_back(prefixsum[index].back() + (s[i] == c ? 1 : 0));
	}
}

/*
 * Complete the 'answerQuery' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER l
 *  2. INTEGER r
 */

int answerQuery(int l, int r)
{
	// Return the answer for this query modulo 1000000007.
	int single = 0, len = 0;
	vector<int> sum; //sum of each char in range [l,r]
	for (char c = 'a'; c <= 'z'; ++c)
	{
		int index = c - 'a';
		int count = prefixsum[index][r] - prefixsum[index][l - 1];
		single += count % 2;
		sum.push_back(count / 2);
		len += count / 2;
	}
	long res = 1;
	if (single != 0)
		res = single;
	res = res * fac[len] % MOD;
	for (auto n : sum)
	{
		if (n == 0)
			continue;
		res = res * ifac[n] % MOD;
		len -= n;
	}
	return res;
}