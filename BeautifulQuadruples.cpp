#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

long long beautifulQuadruples(int a, int b, int c, int d)
{
	long long res = 0, count = 0;
	vector<int> v = {a, b, c, d};
	sort(v.begin(), v.end());
	unordered_map<int, int> m;
	for (int i = v[1] + 1; i <= v[2]; ++i)
	{
		for (int j = i; j <= v[3]; ++j)
		{
			++m[i ^ j];
			++count;
		}
	}
	for (int i = v[1]; i >= 1; --i)
	{
		for (int j = i; j <= v[3]; ++j)
		{
			++m[i ^ j];
			++count;
		}
		for (int j = 1; j <= min(v[0], i); ++j)
		{
			res += count - m[i ^ j];
		}
	}
	return res;
}