#include <vector>
using namespace std;

// const value
static const int MOD = 1e9 + 7;

void postorder(vector<vector<int>> &graph, int at, int from, vector<pair<long, long>> &dp)
{
	long safe = 1, unsafe = 1;
	if (dp[at].first != -1)
		return;
	for (auto to : graph[at])
	{
		if (to == from)
			continue;
		postorder(graph, to, at, dp);

		unsafe = (unsafe * dp[to].first) % MOD;
		safe = safe * (dp[to].first * 2 + dp[to].second) % MOD; //double safe to extract unsafe
	}
	safe = (safe - unsafe + MOD) % MOD;
	dp[at] = {safe, unsafe};
}

int kingdomDivision(int n, vector<vector<int>> roads)
{
	vector<vector<int>> graph(n + 1);
	vector<pair<long, long>> dp(n + 1, {-1, -1}); //{safe,unsafe}
	for (auto &road : roads)
	{
		graph[road[0]].push_back(road[1]);
		graph[road[1]].push_back(road[0]);
	}

	postorder(graph, 1, -1, dp);
	return dp[1].first * 2 % MOD;
}