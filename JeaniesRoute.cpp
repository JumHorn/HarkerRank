#include <algorithm>
#include <vector>
using namespace std;

/*
Steiner tree problem
由于这个问题的图是树状结构，从一个节点到另一个节点只有一条路径
所以斯坦纳树的最小生成树，直接连接各个节点即可

那么最后的路径是多少
按照欧拉一笔画问题，从起点必须到终点，那么路径必须是最小生成树的2倍
该问题不需要回到起点，
那么两两节点之间只有一条路径不需要经过，当然是最长的路径不经过最好
要求树的直径
*/

/*
该问题编码时出现了很多问题，应该自己设置test case自测，一般都是变量指代错误
*/

//{longest path,depth}
pair<int, int> diameter(vector<vector<pair<int, int>>> &graph, int at, int from, vector<int> &land)
{
	int path = -1, depth1 = 0, depth2 = 0;
	if (land[at] == 1)
		path = 0;
	for (auto p : graph[at])
	{
		int to = p.first, w = p.second;
		if (to == from)
			continue;
		auto r = diameter(graph, to, at, land);
		if (r.first != -1)
		{
			if (r.second + w > depth1)
			{
				depth2 = depth1;
				depth1 = r.second + w;
			}
			else if (r.second + w > depth2)
				depth2 = r.second + w;

			path = max(path, r.first);
			if (depth2 > 0)
				path = max(path, depth1 + depth2);
			if (depth1 > 0 && land[at] == 1)
				path = max(path, depth1);
		}
	}
	return {path, max(depth1, depth2)};
}

//{letter city count,weight}
pair<int, int> postorder(vector<vector<pair<int, int>>> &graph, int at, int from, vector<int> &land, int landcount)
{
	int city = 0, weight = 0;
	for (auto p : graph[at])
	{
		int to = p.first, w = p.second;
		if (to == from)
			continue;
		auto r = postorder(graph, to, at, land, landcount);
		weight += r.second;
		if (r.first < landcount && r.first > 0)
		{
			weight += w;
			city += r.first;
		}
	}
	if (land[at] == 1)
		++city;
	return {city, weight};
}

/*
 * Complete the jeanisRoute function below.
 */
int jeanisRoute(vector<int> k, vector<vector<int>> roads)
{
	int N = roads.size() + 1;
	vector<vector<pair<int, int>>> graph(N + 1);
	for (auto &r : roads)
	{
		graph[r[0]].push_back({r[1], r[2]});
		graph[r[1]].push_back({r[0], r[2]});
	}
	vector<int> land(N + 1);
	for (auto n : k)
		land[n] = 1;
	int sum = postorder(graph, 1, -1, land, k.size()).second;
	int d = diameter(graph, 1, -1, land).first;
	return 2 * sum - d;
}