#include <set>
#include <vector>
using namespace std;

int activityNotifications(vector<int> nums, int k)
{
	int res = 0;
	multiset<int> s(nums.begin(), nums.begin() + k);
	multiset<int>::iterator mid = next(s.begin(), k / 2);
	for (int i = k;; ++i)
	{
		double n = ((double)*mid + *prev(mid, 1 - k % 2)) / 2.0;
		if (nums[i] >= 2 * n)
			++res;
		if (i == (int)nums.size())
			break;
		s.insert(nums[i]);
		if (nums[i] < *mid)
			--mid;
		if (nums[i - k] <= *mid)
			++mid;
		s.erase(s.find(nums[i - k]));
	}
	return res;
}