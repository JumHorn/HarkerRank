#include <vector>
using namespace std;

bool backTracking(int n, int k, int index, vector<int> &seen, vector<int> &res)
{
	if (index > n)
		return true;
	int arr[] = {index - k, index + k};
	for (int i = 0; i < 2; ++i)
	{
		int num = arr[i];
		if (num > n || num <= 0)
			continue;
		if (seen[num] == 0)
		{
			res.push_back(num);
			seen[num] = 1;
			if (backTracking(n, k, index + 1, seen, res))
				return true;
			seen[num] = 0;
			res.pop_back();
		}
	}
	return false;
}

vector<int> absolutePermutation(int n, int k)
{
	if (k == 0)
	{
		vector<int> res;
		for (int i = 1; i <= n; ++i)
			res.push_back(i);
		return res;
	}
	vector<int> seen(n + 1), res;
	if (backTracking(n, k, 1, seen, res))
		return res;
	return {-1};
}