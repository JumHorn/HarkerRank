#include <iostream>
#include <string>
using namespace std;

pair<int, int> range(int n)
{
	int lo = 1, hi = n;
	while (lo < hi)
	{
		int mi = (hi - lo) / 2 + lo;
		if (mi * mi < n)
			lo = mi + 1;
		else
			hi = mi;
	}
	if (lo * lo == n || lo * (lo - 1) < n)
		return {lo, lo};
	return {lo - 1, lo};
}

string trimSpace(string &s)
{
	string res;
	for (auto c : s)
	{
		if (c != ' ')
			res.push_back(c);
	}
	return res;
}

string encryption(string s)
{
	s = trimSpace(s);
	int N = s.length();
	auto p = range(N);
	int r = p.first, c = p.second;
	string res;
	for (int j = 0; j < c; ++j)
	{
		for (int i = 0; i < r; ++i)
		{
			if (i * c + j < N)
				res += s[i * c + j];
		}
		res += ' ';
	}
	return res;
}