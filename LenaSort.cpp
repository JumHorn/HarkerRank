#include <iostream>
#include <vector>
using namespace std;

/*
https://www.hackerrank.com/challenges/lena-sort/editorial
该问题建立了二叉树的模型
并解释了从lower到upper区间所有的比较次数都是可能的
非常恐怖的建模问题
*/

/*
我的普通构造方法也通过了
只是又一次的错在了数据限制上，改成long long就通过了
*/

class LenaSort
{
public:
	LenaSort()
	{
		assert(N >= 3);
		lower[2] = upper[2] = 1;
		for (int i = 3; i < N; ++i)
		{
			//lower bound
			lower[i] = lower[i / 2] + lower[i - 1 - i / 2];
			lower[i] += i - 1;

			//upper bound
			upper[i] = (long long)i * (i - 1) / 2;
		}
	}

	// [l,r],number of comparisons
	bool constructArray(int l, int r, long long c, vector<int> &res)
	{
		int len = r - l + 1;
		if (c < lower[len] || c > upper[len])
			return false;
		if (len == 0)
			return true;
		c -= len - 1;
		for (int pivot = l; pivot <= r; ++pivot)
		{
			int len1 = pivot - l, len2 = r - pivot;
			for (long long c1 = max(lower[len1], c - upper[len2]); c1 <= min(upper[len1], c - lower[len2]); ++c1)
			{
				long long c2 = c - c1;
				res.push_back(pivot);
				if (constructArray(l, pivot - 1, c1, res) && constructArray(pivot + 1, r, c2, res))
					return true;
				res.pop_back();
			}
		}
		return false;
	}

private:
	static vector<long long> lower, upper;
	static const int N = 1e5 + 1;
};

vector<long long> LenaSort::lower = vector<long long>(N);
vector<long long> LenaSort::upper = vector<long long>(N);