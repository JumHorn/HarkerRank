#include <string>
#include <vector>
using namespace std;

/*
注意下标的对应关系，每次调试都是下标对应关系出错
*/

// const value
static const int MOD = 1e9 + 7;

int substrings(string n)
{
	int N = n.size();
	vector<long> dp(N + 1);
	dp[0] = 1;
	for (int i = 1; i <= N; ++i)
		dp[i] = dp[i - 1] * 10 % MOD;
	//prefix sum
	for (int i = 1; i <= N; ++i)
		dp[i] = (dp[i] + dp[i - 1]) % MOD;

	long long res = 0;
	for (int i = 0; i < N; ++i)
		res = (res + dp[N - i - 1] * (i + 1) * (n[i] - '0') % MOD) % MOD;
	return res;
}