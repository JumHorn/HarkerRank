#include <algorithm>
#include <vector>
using namespace std;

/*
看到abs绝对值问题就十分麻烦，去除绝对值是常规想法
用排序决定元素大小顺序就可以避免讨论

dp[i][j]表示前[0,i]个元素中取出j个元素放在A集合的最小值
这个定义不准确，但是具体描述起来又会增加复杂度，暂且这样描述使得代码更容易完成

在初始化的时候
dp[i + 1][0] = dp[i][0] - arr[i] * k;
提前就减去了将arr[i]放到B集合对最终结果的影响
所以对于dp的中间过程并不是局部最终结果

当然该问题再次出现溢出
两数相乘，相加一定要注意溢出
*/

long fairCut(int k, vector<int> arr)
{
	sort(arr.begin(), arr.end());
	int N = arr.size();
	k = (N - k < k ? N - k : k);
	vector<vector<long>> dp(N + 1, vector<long>(k + 1, LONG_MAX / 2));

	dp[0][0] = 0;
	for (int i = 0; i < N; ++i)
		dp[i + 1][0] = dp[i][0] - (long)arr[i] * k;

	for (int i = 0; i < N; ++i)
	{
		long a_i = arr[i];
		for (int j = 0; j < min(k, i + 1); ++j)
		{
			//put a_i in A
			long measure_A = dp[i][j] - a_i * ((N - k) - (i - j)) + a_i * (i - j);
			//put a_i in B
			long measure_B = dp[i][j + 1] - a_i * (k - j - 1) + a_i * (j + 1);
			dp[i + 1][j + 1] = min(measure_A, measure_B);
		}
	}
	return dp[N][k];
}