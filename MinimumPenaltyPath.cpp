#include <algorithm>
#include <queue>
#include <vector>
using namespace std;

//bfs
int bfs(vector<vector<pair<int, int>>> &graph, int source, int target)
{
	int N = graph.size();
	vector<vector<bool>> distance(N, vector<bool>(1 << 10));
	distance[source][0] = true;
	queue<pair<int, int>> q; //{distance,node}
	q.push({0, source});
	while (!q.empty())
	{
		auto top = q.front();
		q.pop();
		for (auto &p : graph[top.second])
		{
			int node = p.first, weight = p.second;
			int w = (weight | top.first);
			if (!distance[node][w])
			{
				distance[node][w] = true;
				q.push({w, node});
			}
		}
	}

	for (int i = 0; i < (1 << 10); ++i)
	{
		if (distance[target][i])
			return i;
	}
	return -1;
}

int beautifulPath(vector<vector<int>> edges, int n, int A, int B)
{
	vector<vector<pair<int, int>>> graph(n + 1);
	for (auto &edge : edges)
	{
		graph[edge[0]].push_back({edge[1], edge[2]});
		graph[edge[1]].push_back({edge[0], edge[2]});
	}
	auto d = bfs(graph, A, B);
	return d;
}