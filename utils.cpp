#include <algorithm>
#include <iomanip>
#include <iostream>
#include <set>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;

// vocabulary
const char *NUM[] = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
					 "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty"};
const char *WEEK[] = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
const char *MONTH[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

// const value
static const int MOD = 1e9 + 7;

//board dfs direction
int path[5] = {-1, 0, 1, 0, -1};

vector<string> split(const string &s, const string &sep = " ")
{
	int N = s.length();
	bool delimter[128] = {0};
	for (auto c : sep)
		delimter[c] = true;
	vector<string> res;
	for (int i = 0, j = 0; i <= N; ++i)
	{
		if (i == N || delimter[s[i]])
		{
			res.push_back(s.substr(j, i - j));
			j = i + 1;
		}
	}
	return res;
}

// generate prime in [2,n]
vector<int> generatePrime(int n)
{
	assert(n > 1);
	vector<bool> primer(n + 1);
	for (int i = 2; i <= n; ++i)
	{
		if (primer[i])
			continue;
		for (int j = i + i; j <= n; j += i)
			primer[j] = true;
	}
	vector<int> res;
	for (int i = 2; i <= n; ++i)
	{
		if (!primer[i])
			res.push_back(i);
	}
	return res;
}

// overload
ostream &operator<<(ostream &os, vector<int> &v)
{
	for (auto n : v)
		os << n << " ";
	return os << endl;
}

// speed up tips
int speed_up()
{
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	return 0;
}

int static r = speed_up(); //make it call before main

#define endl '\n'; // prevents flushing buffer for each line
/********end of speed up tips********/

// easy implement big decimal for big integer use
// not accept negative number now
class BigDecimal
{
public:
	BigDecimal() : data("0") //default for 0
	{
	}

	BigDecimal(const string &num) : data(num) //no check for invalid string
	{
		reverse(data.begin(), data.end());
	}

	BigDecimal(int num)
	{
		data = to_string(num);
		reverse(data.begin(), data.end());
	}

	// for print
	friend ostream &operator<<(ostream &out, const BigDecimal &biginteger)
	{
		for (auto it = biginteger.data.rbegin(); it != biginteger.data.rend(); ++it)
			out << *it;
		return out;
	}

	//not effecient use BigInteger instead
	BigDecimal &operator*=(const BigDecimal &other)
	{
		auto &num1 = this->data, num2 = other.data;
		reverse(num1.begin(), num1.end());
		reverse(num2.begin(), num2.end());
		int N1 = num1.length(), N2 = num2.length();
		string res(N1 + N2, '0');
		for (int i = N1 - 1; i >= 0; --i)
		{
			for (int j = N2 - 1; j >= 0; --j)
			{
				int sum = (num1[i] - '0') * (num2[j] - '0') + (res[i + j + 1] - '0');
				res[i + j + 1] = sum % 10 + '0';
				res[i + j] += sum / 10;
			}
		}
		for (int i = 0; i < N1 + N2; ++i)
		{
			if (res[i] != '0')
			{
				data = res.substr(i);
				reverse(data.begin(), data.end());
				return *this;
			}
		}
		data = "0";
		return *this;
	}

	const BigDecimal operator*(const BigDecimal &other)
	{
		BigDecimal res = *this;
		return res *= other;
	}

	BigDecimal &operator+=(const BigDecimal &other)
	{
		auto &num1 = this->data;
		auto &num2 = other.data;
		int N1 = num1.length(), N2 = num2.length();
		for (int i = 0, j = 0, carry = 0; i < N1 || j < N2 || carry > 0; carry /= 10)
		{
			if (j < N2)
				carry += num2[j++] - '0';

			if (i < N1)
			{
				carry += num1[i] - '0';
				num1[i++] = carry % 10 + '0';
			}
			else
				num1.push_back(carry % 10 + '0');
		}
		return *this;
	}

	const BigDecimal operator+(const BigDecimal &other)
	{
		BigDecimal res = *this;
		return res += other;
	}

private:
	string data; //data reversed stored
};
/********end of BigDecimal********/

// easy implement big integer with data reversed stored
// not accept negative number now
class BigInteger
{
public:
	BigInteger() : data(0) //default for 0
	{
	}

	BigInteger(const vector<int> &num) : data(num) //no check for invalid
	{
		reverse(data.begin(), data.end());
	}

	BigInteger(int num)
	{
		data = {num};
	}

	// for print
	friend ostream &operator<<(ostream &out, const BigInteger &biginteger)
	{
		auto &data = biginteger.data;
		int N = data.size();
		out << data[N - 1];
		for (int i = N - 2; i >= 0; --i)
			out << setfill('0') << setw(9) << right << data[i];
		return out;
	}

	BigInteger &operator*=(const BigInteger &other)
	{
		auto &num1 = this->data;
		auto &num2 = other.data;
		int N1 = num1.size(), N2 = num2.size();
		vector<int> res(N1 + N2);
		for (int i = 0; i < N1; ++i)
		{
			for (int j = 0; j < N2; ++j)
			{
				long long sum = (long long)num1[i] * num2[j] + res[i + j];
				res[i + j] = (sum % MASK);
				res[i + j + 1] += (sum / MASK);
			}
		}
		//remove prefix zero
		while (res.size() > 1 && res.back() == 0)
			res.pop_back();
		data = res;
		return *this;
	}

	BigInteger operator*(const BigInteger &other)
	{
		BigInteger res = *this;
		return res *= other;
	}

	BigInteger &operator+=(const BigInteger &other)
	{
		auto &num1 = this->data;
		auto &num2 = other.data;
		int N1 = num1.size(), N2 = num2.size();
		long long carry = 0;
		for (int i = 0, j = 0, carry = 0; i < N1 || j < N2 || carry > 0; carry /= MASK)
		{
			if (j < N2)
				carry += num2[j++];

			if (i < N1)
			{
				carry += num1[i];
				num1[i++] = (carry % MASK);
			}
			else
				num1.push_back(carry % MASK);
		}
		return *this;
	}

	BigInteger operator+(const BigInteger &other)
	{
		BigInteger res = *this;
		return res += other;
	}

	int getlen()
	{
		return data.size();
	}

private:
	vector<int> data; //every int store 1000000000
	static const int BIT = 31;
	static const int MASK = 1e9;
};
/********end of BigInteger********/

// DSU
class DSU
{
public:
	DSU(int size) : parent(size), rank(size, 1)
	{
		for (int i = 0; i < size; ++i)
			parent[i] = i;
	}

	int Find(int x)
	{
		if (x != parent[x])
			parent[x] = Find(parent[x]);
		return parent[x];
	}

	bool Union(int x, int y)
	{
		int xr = Find(x), yr = Find(y);
		if (xr == yr)
			return false;
		parent[yr] = xr;
		rank[xr] += rank[yr];
		return true;
	}

	int Count(int x)
	{
		return rank[Find(x)];
	}

private:
	vector<int> parent;
	vector<int> rank;
};
/********end of DSU********/