#include <iostream>
#include <string>
#include <vector>
using namespace std;

void kMarsh(vector<string> grid)
{
	int M = grid.size(), N = grid[0].size();
	vector<vector<int>> left(M, vector<int>(N));
	vector<vector<int>> right(M, vector<int>(N));
	vector<vector<int>> up(M, vector<int>(N));
	vector<vector<int>> down(M, vector<int>(N));

	for (int i = 0; i < M; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			if (grid[i][j] == '.')
				left[i][j] = 1 + (j > 0 ? left[i][j - 1] : 0);
		}
	}
	for (int i = 0; i < M; ++i)
	{
		for (int j = N - 1; j >= 0; --j)
		{
			if (grid[i][j] == '.')
				right[i][j] = 1 + (j < N - 1 ? right[i][j + 1] : 0);
		}
	}
	for (int j = 0; j < N; ++j)
	{
		for (int i = 0; i < M; ++i)
		{
			if (grid[i][j] == '.')
				up[i][j] = 1 + (i > 0 ? up[i - 1][j] : 0);
		}
	}
	for (int j = 0; j < N; ++j)
	{
		for (int i = M - 1; i >= 0; --i)
		{
			if (grid[i][j] == '.')
				down[i][j] = 1 + (i < M - 1 ? down[i + 1][j] : 0);
		}
	}
	int res = -1;
	for (int i = 0; i < M; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			if (grid[i][j] == 'X')
				continue;

			for (int r = i + down[i][j] - 1; r > i; --r)
			{
				for (int c = j + right[i][j] - 1; c > j; --c)
				{
					if (2 * (r - i + c - j) <= res) //very important optimization
						break;
					if (right[i][j] > c - j && down[i][j] > r - i &&
						left[r][c] > c - j && up[r][c] > r - i)
						res = max(res, 2 * (r - i + c - j));
				}
			}
		}
	}

	if (res == -1)
		cout << "impossible" << endl;
	else
		cout << res << endl;
}