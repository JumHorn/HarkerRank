#include <string>
#include <vector>
using namespace std;

/*
5000*5000的计算量是不会超时的
以下代码思路来自editorial
一共62个字符，要减少常数参数

copy代码容易出错，那么代码的封装就是有问题的
*/

int index(char c)
{
	if (c >= 'a' && c <= 'z')
		return c - 'a';
	if (c >= 'A' && c <= 'Z')
		return c - 'A' + 26;
	return c - '0' + 52;
}

int tutzkiAndLcs(string a, string b)
{
	int M = a.size(), N = b.size();
	// left lcs
	vector<vector<int>> dpl(M + 2, vector<int>(N + 2));
	for (int i = 0; i < M; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			if (a[i] == b[j])
				dpl[i + 1][j + 1] = dpl[i][j] + 1;
			else
				dpl[i + 1][j + 1] = max(dpl[i][j + 1], dpl[i + 1][j]);
		}
	}

	// right lcs
	vector<vector<int>> dpr(M + 2, vector<int>(N + 2));
	for (int i = M - 1; i >= 0; --i)
	{
		for (int j = N - 1; j >= 0; --j)
		{
			if (a[i] == b[j])
				dpr[i + 1][j + 1] = dpr[i + 2][j + 2] + 1;
			else
				dpr[i + 1][j + 1] = max(dpr[i + 1][j + 2], dpr[i + 2][j + 1]);
		}
	}

	vector<vector<int>> pos(62);
	for (int i = 0; i < N; ++i)
		pos[index(b[i])].push_back(i);
	int lcs = dpl[M][N], res = 0;
	//insering character between position i and i+1
	for (int i = 0; i <= M; ++i)
	{
		for (int c = 0; c < 62; ++c)
		{
			for (auto j : pos[c])
			{
				if (dpl[i][j] + dpr[i + 1][j + 2] == lcs)
				{
					++res;
					break;
				}
			}
		}
	}
	return res;
}