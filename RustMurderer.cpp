#include <queue>
#include <unordered_set>
#include <vector>
using namespace std;

/*
BFS on complementary graph
互补的图
*/

vector<int> rustMurdered(int n, vector<vector<int>> roads, int start)
{
	vector<unordered_set<int>> graph(n + 1);
	for (auto &r : roads)
	{
		graph[r[0]].insert(r[1]);
		graph[r[1]].insert(r[0]);
	}
	unordered_set<int> undiscovered;
	for (int i = 1; i <= n; ++i)
		undiscovered.insert(i);

	//bfs
	vector<int> res(n);
	queue<int> q;
	q.push(start);
	res[start - 1] = 0;
	undiscovered.erase(start);
	int dist = 0;
	while (!q.empty())
	{
		++dist;
		int size = q.size();
		while (--size >= 0)
		{
			int node = q.front();
			q.pop();
			vector<int> complement_vertex;
			for (auto n : undiscovered)
			{
				if (graph[node].count(n) == 0)
					complement_vertex.push_back(n);
			}

			for (auto n : complement_vertex)
			{
				undiscovered.erase(n);
				q.push(n);
				res[n - 1] = dist;
			}
		}
	}

	res.erase(res.begin() + start - 1);
	return res;
}