#include <vector>
using namespace std;

/*
A[i]只能取B[i]的最大值或者最小值1
如何证明这种方法全局最优

证明
假设 存在一组[a0, a1, a2,...,an]最终结果最大
考虑其中一个三元组
[a0, a1, a2]
如果a1>1并且 a1<B[1] 即a1不是最大值也不是最小值

情况一：
a1>=a0 并且 a1>=a2 abs(a1-a0)+abs(a1-a2) < abs(B[i]-a0) + abs(B[i]-a2)
即a1可以取B[i]使得最终结果更大

情况二：
a1<a0 并且 a1<a2,同上
即a1可以取1使得最终结果更大

情况三：设a0<a2 当a0>a2时同理
此时和是数轴上a0-a2的距离
a0|<-----a1----->|a2

a0|<-----a1'----->|a2
如果最大值最小值都在区间[a0,a2]那么取区间中任意一点最终结果相同
如果如果最大值最小值不在区间[a0,a2]，那么取区间外一点，总比区间内的结果大

*/

int cost(vector<int> B)
{
	int N = B.size();
	vector<pair<int, int>> dp(N); //{min,max}
	for (int i = 1; i < N; ++i)
	{
		dp[i].first = max(dp[i - 1].first, abs(1 - B[i - 1]) + dp[i - 1].second);
		dp[i].second = max(abs(B[i] - 1) + dp[i - 1].first, abs(B[i] - B[i - 1]) + dp[i - 1].second);
	}
	return max(dp.back().first, dp.back().second);
}