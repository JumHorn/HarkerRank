#include <string>
#include <vector>
using namespace std;

vector<vector<int>> largestPlusSign(vector<string> &grid)
{
	int M = grid.size(), N = grid[0].size();
	int count = 0;
	vector<vector<int>> dp(M, vector<int>(N));
	for (int i = 0; i < M; ++i)
	{
		count = 0;
		for (int j = 0; j < N; ++j)
		{
			count = grid[i][j] == 'B' ? 0 : count + 1;
			dp[i][j] = count;
		}
		count = 0;
		for (int j = N - 1; j >= 0; --j)
		{
			count = grid[i][j] == 'B' ? 0 : count + 1;
			dp[i][j] = min(dp[i][j], count);
		}
	}

	int res = 0;
	for (int j = 0; j < N; ++j)
	{
		count = 0;
		for (int i = 0; i < M; ++i)
		{
			count = grid[i][j] == 'B' ? 0 : count + 1;
			dp[i][j] = min(dp[i][j], count);
		}
		count = 0;
		for (int i = M - 1; i >= 0; --i)
		{
			count = grid[i][j] == 'B' ? 0 : count + 1;
			dp[i][j] = min(dp[i][j], count);
		}
	}
	return dp;
}

bool overlap(int M, int N, int r1, int c1, int size1, int r2, int c2, int size2)
{
	int grid[M][N];
	//board dfs direction
	int path[5] = {-1, 0, 1, 0, -1};
	memset(grid, 0, sizeof(grid));
	for (int i = 0; i < size1; ++i)
	{
		for (int j = 0; j < 4; ++j)
			grid[r1 + path[j] * i][c1 + path[j + 1] * i] = 1;
	}
	for (int i = 0; i < size2; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			if (grid[r2 + path[j] * i][c2 + path[j + 1] * i] == 1)
				return true;
		}
	}
	return false;
}

int twoPluses(vector<string> grid)
{
	auto dp = largestPlusSign(grid);
	int M = grid.size(), N = grid[0].size();
	int res = 0;
	for (int i = 0; i < M * N; ++i)
	{
		int r1 = i / N, c1 = i % N;
		if (dp[r1][c1] == 0)
			continue;
		for (int j = i + 1; j < M * N; ++j)
		{
			int r2 = j / N, c2 = j % N;
			if (dp[r2][c2] == 0)
				continue;
			for (int size1 = dp[r1][c1]; size1 > 0; --size1)
			{
				for (int size2 = dp[r2][c2]; size2 > 0; --size2)
				{
					if (!overlap(M, N, r1, c1, size1, r2, c2, size2))
						res = max(res, (size1 * 4 - 3) * (size2 * 4 - 3));
				}
			}
		}
	}
	return res;
}