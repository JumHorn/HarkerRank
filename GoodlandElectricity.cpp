#include <map>
#include <vector>
using namespace std;

int pylons(int k, vector<int> arr)
{
	map<int, int> range; //[l,r) for each index with 1
	int N = arr.size();
	for (int i = 0; i < N; ++i)
	{
		if (arr[i] == 1)
			range[max(i - k + 1, 0)] = i + k;
	}
	if (range.begin()->first > 0 || range.rbegin()->second < N)
		return -1;
	int res = 0, curright = -1, maxright = range.begin()->second;
	for (auto &p : range)
	{
		int l = p.first, r = p.second;
		if (curright >= N)
			break;
		if (l > maxright)
			return -1;
		if (l > curright)
		{
			++res;
			curright = maxright;
		}
		maxright = max(maxright, r);
	}
	if (curright < N)
		++res;
	return res;
}