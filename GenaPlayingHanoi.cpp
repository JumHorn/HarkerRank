#include <queue>
#include <unordered_set>
#include <vector>
using namespace std;

/*
BFS to solve this problem
change post to bitset state
each value in posts will be represent in 2 bits in order
N=len(post) and N<=10
only N*20 bits is enough
*/

int compressState(vector<int> &posts)
{
	int state = 0, N = posts.size();
	for (int i = 0; i < N; ++i)
		state |= ((posts[i] - 1) << (i << 1));
	return state;
}

// n : length of state
int produceFinalState(int n)
{
	// int state = 0;
	// for (int i = 0; i < n; ++i)
	// 	state |= (1u << (i << 1));
	// return state;
	return 0;
}

vector<vector<int>> stateToHani(int state, int n)
{
	vector<vector<int>> hanoi(4);
	for (int i = n; i >= 1; --i)
		hanoi[(state >> ((i - 1) << 1)) & 0b11].push_back(i);
	return hanoi;
}

int hanoiToState(vector<vector<int>> &hanoi)
{
	int state = 0;
	for (int i = 0; i < (int)hanoi.size(); ++i)
	{
		for (auto n : hanoi[i])
			state |= (i << ((n - 1) << 1));
	}
	return state;
}

int hanoi(vector<int> posts)
{
	unordered_set<int> seen; //{state mask already visited}
	queue<int> q;			 //state mask
	int N = posts.size();
	int initialstate = compressState(posts);
	int finalstate = produceFinalState(N);

	//BFS
	int res = 0;
	q.push(initialstate);
	seen.insert(initialstate);
	while (!q.empty())
	{
		++res;
		int size = q.size();
		while (--size >= 0)
		{
			int state = q.front();
			q.pop();
			vector<vector<int>> hanoiArr = stateToHani(state, N);
			for (int i = 0; i < 4; ++i)
			{
				if (hanoiArr[i].empty())
					continue;
				for (int j = 0; j < 4; ++j)
				{
					if (i == j)
						continue;
					// can not move to the larger one
					if (!hanoiArr[j].empty() && hanoiArr[j].back() < hanoiArr[i].back())
						continue;

					int val = hanoiArr[i].back();
					hanoiArr[i].pop_back();
					hanoiArr[j].push_back(val);

					int next_state = hanoiToState(hanoiArr);
					if (next_state == finalstate)
						return res;
					if (seen.count(next_state) == 0)
					{
						seen.insert(next_state);
						q.push(next_state);
					}

					val = hanoiArr[j].back();
					hanoiArr[j].pop_back();
					hanoiArr[i].push_back(val);
				}
			}
		}
	}
	return -1;
}