#include <string>
#include <vector>
using namespace std;

/*
assume the index of abcd's value is xyyx
then for j in(1,N)
dp[26] means number of x before index j
dp[26][26] means number of xy before index j
dp[26][26][26] means number of xyy before index j
dp[26][26][26][26] means number of xyyx before index j
*/

int shortPalindrome(string s)
{
	static const int MOD = 1e9 + 7;
	int dp1[26];
	int dp2[26][26];
	int dp3[26][26][26];
	int dp4[26][26][26][26];
	memset(dp1, 0, sizeof(dp1));
	memset(dp2, 0, sizeof(dp2));
	memset(dp3, 0, sizeof(dp3));
	memset(dp4, 0, sizeof(dp4));
	for (auto c : s)
	{
		int i = c - 'a';
		for (int j = 0; j < 26; ++j)
		{
			dp4[i][j][j][i] += dp3[i][j][j];
			dp3[j][i][i] += dp2[j][i];
			dp2[j][i] += dp1[j];

			dp4[i][j][j][i] %= MOD;
			dp3[j][i][i] %= MOD;
			dp2[j][i] %= MOD;
		}
		++dp1[i];
	}

	int res = 0;
	for (int i = 0; i < 26; ++i)
	{
		for (int j = 0; j < 26; ++j)
			res = (res + dp4[i][j][j][i]) % MOD;
	}
	return res;
}