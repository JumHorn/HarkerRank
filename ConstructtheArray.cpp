#include <vector>
using namespace std;

/*
对于排列组合问题的递推公式，在数学中不能计算，但是对于计算机是可以计算这些公式的
之前的排列组合总是运用的不是很好
*/

long countArray(int n, int k, int x)
{
	// const value
	static const int MOD = 1e9 + 7;
	vector<pair<long, long>> dp(n); //{last is x,last is not x}
	dp[1].first = (x == 1 ? 0 : 1);
	dp[1].second = (x == 1 ? k - 1 : k - 2);
	for (int i = 2; i < n; ++i)
	{
		dp[i].first = dp[i - 1].second;
		dp[i].second = (dp[i - 1].first * (k - 1) + dp[i - 1].second * (k - 2)) % MOD;
	}
	return dp[n - 1].first;
}
