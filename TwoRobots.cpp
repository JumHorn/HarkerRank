#include <algorithm>
#include <cmath>
#include <vector>
using namespace std;

/*
思路来自leetcode
https://leetcode.com/problems/minimum-distance-to-type-a-word-using-two-fingers/discuss/477652/JavaC%2B%2BPython-1D-DP-O(1)-Space
dp[i]表示前一个节点在b时可以节省下的最多距离
*/

int twoRobots(int m, vector<vector<int>> &queries)
{
	vector<int> dp(m + 1);
	int res = 0, alldist = abs(queries[0][0] - queries[0][1]);
	for (int i = 0; i < (int)queries.size() - 1; ++i)
	{
		int a = queries[i][0], b = queries[i][1], c = queries[i + 1][0], d = queries[i + 1][1];
		for (int j = 1; j <= m; ++j)
			dp[b] = max(dp[b], dp[j] + abs(b - c) - abs(j - c));
		alldist += abs(b - c) + abs(c - d);
	}
	return alldist - *max_element(dp.begin(), dp.end());
}