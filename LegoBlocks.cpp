#include <vector>
using namespace std;

/*
算法思路
先计算一行的情况 f[i]表示1行长度为i的墙的所有可能性
再计算n行所有的可能行 g[i]表示n行长度为i的墙所有可能性 g[i]=pow(f[i],i)
再计算去除非法的情况的可能性 h[i]表示n行长度为i的合法可能性
h[i] = g[i] - {sum(h[j]*g[i-j])(1<=j<i)}
{sum(h[j]*g[i-j])(1<=j<i)}表示非法可能性
原理是
对于j列，如果这一列贯穿整个墙
那么h[j]表示这列以前的都合法，g[i-j]表示这列之后的所有可能性
那么由于这列贯穿整个墙，这些可能性都非法，所以要排除掉

公式过于精妙，本题理解即可
*/

/*
bug查找时刻
该问题又出现了常见bug
错误写法
h[i] = (h[i] - h[j] * g[i - j] + MOD) % MOD
错误原因减法溢出

正确写法
h[i] = (h[i] - h[j] * g[i - j] % MOD + MOD) % MOD
*/

// const value
static const int MOD = 1e9 + 7;

// pow with mod
long long modPow(long long x, int n)
{
	long long res = 1;
	for (auto i = n; i > 0; i /= 2)
	{
		if (i % 2)
			res = res * x % MOD;
		x = x * x % MOD;
	}
	return res;
}

int legoBlocks(int n, int m)
{
	//denote number of ways to cover a 1Xi bar
	vector<long> f(m + 1);
	f[0] = 1;
	for (int i = 1; i <= m; ++i)
	{
		for (int j = 1; j <= 4; ++j)
		{
			if (i >= j)
				f[i] = (f[i] + f[i - j]) % MOD;
		}
	}

	//denote number of ways to cover a nXi bar without solid condition
	vector<long> g(m + 1);
	for (int i = 0; i <= m; ++i)
		g[i] = modPow(f[i], n);
	//denote number of ways to cover a nXi bar with solid condition
	vector<long> h(m + 1);
	h[1] = 1;
	for (int i = 2; i <= m; ++i)
	{
		h[i] = g[i];
		for (int j = 1; j < i; ++j)
			h[i] = (h[i] - h[j] * g[i - j] % MOD + MOD) % MOD;
	}
	return h[m];
}