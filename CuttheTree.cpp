#include <numeric>
#include <vector>
using namespace std;

int postorder(vector<vector<int>> &graph, vector<int> &data, int at, int from, int &sum, int &res)
{
	int count = data[at - 1];
	for (auto to : graph[at])
	{
		if (from != to)
			count += postorder(graph, data, to, at, sum, res);
	}
	if (at != 1)
		res = min(res, abs(sum - 2 * count));
	return count;
}

int cutTheTree(vector<int> data, vector<vector<int>> edges)
{
	int sum = accumulate(data.begin(), data.end(), 0);
	int N = data.size();
	vector<vector<int>> graph(N + 1);
	for (auto &edge : edges)
	{
		graph[edge[0]].push_back(edge[1]);
		graph[edge[1]].push_back(edge[0]);
	}
	int res = INT_MAX;
	postorder(graph, data, 1, -1, sum, res);
	return res;
}