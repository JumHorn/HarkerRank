#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

// easy implement big integer with data reversed stored
// not accept negative number now
class BigInteger
{
public:
	BigInteger() : data(0) //default for 0
	{
	}

	BigInteger(const vector<int> &num) : data(num) //no check for invalid
	{
		reverse(data.begin(), data.end());
	}

	BigInteger(int num)
	{
		data = {num};
	}

	// for print
	friend ostream &operator<<(ostream &out, const BigInteger &biginteger)
	{
		auto &data = biginteger.data;
		int N = data.size();
		out << data[N - 1];
		for (int i = N - 2; i >= 0; --i)
			out << setfill('0') << setw(9) << right << data[i];
		return out;
	}

	BigInteger &operator*=(const BigInteger &other)
	{
		auto &num1 = this->data;
		auto &num2 = other.data;
		int N1 = num1.size(), N2 = num2.size();
		vector<int> res(N1 + N2);
		for (int i = 0; i < N1; ++i)
		{
			for (int j = 0; j < N2; ++j)
			{
				long long sum = (long long)num1[i] * num2[j] + res[i + j];
				res[i + j] = (sum % MASK);
				res[i + j + 1] += (sum / MASK);
			}
		}
		//remove prefix zero
		while (res.size() > 1 && res.back() == 0)
			res.pop_back();
		data = res;
		return *this;
	}

	BigInteger operator*(const BigInteger &other)
	{
		BigInteger res = *this;
		return res *= other;
	}

	BigInteger &operator+=(const BigInteger &other)
	{
		auto &num1 = this->data;
		auto &num2 = other.data;
		int N1 = num1.size(), N2 = num2.size();
		long long carry = 0;
		for (int i = 0, j = 0, carry = 0; i < N1 || j < N2 || carry > 0; carry /= MASK)
		{
			if (j < N2)
				carry += num2[j++];

			if (i < N1)
			{
				carry += num1[i];
				num1[i++] = (carry % MASK);
			}
			else
				num1.push_back(carry % MASK);
		}
		return *this;
	}

	BigInteger operator+(const BigInteger &other)
	{
		BigInteger res = *this;
		return res += other;
	}

	int getlen()
	{
		return data.size();
	}

private:
	vector<int> data; //every int store 10000
	static const int BIT = 31;
	static const int MASK = 1e9;
};
/********end of BigInteger********/

BigInteger fibonacciModified(int t1, int t2, int n)
{
	BigInteger T1(t1), T2(t2), T3;
	for (int i = 3; i <= n; ++i)
	{
		// T3 = T1 + T2 * T2;
		T3 = 1;
		T3 *= T2;
		T3 *= T2;
		T3 += T1;
		T1 = T2;
		T2 = T3;
	}
	return T2;
}