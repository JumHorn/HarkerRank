#include <map>
#include <vector>
using namespace std;

vector<int> climbingLeaderboard(vector<int> ranked, vector<int> player)
{
	int N = player.size(), R = ranked.size();
	vector<int> res;
	sort(ranked.begin(), ranked.end(), greater<int>());
	map<int, int> m; //{score,rank}
	int rank = 1;
	for (int i = 0, pre = INT_MAX; i <= R; ++i)
	{
		if (ranked[i] < pre)
			m[ranked[i]] = rank++;
		pre = ranked[i];
	}
	for (int i = 0, j = 0; i < N; ++i)
	{
		auto it = m.upper_bound(player[i]);
		if (it == m.end())
			res.push_back(1);
		else
			res.push_back(it->second + 1);
	}
	return res;
}